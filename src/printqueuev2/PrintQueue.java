/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package printqueuev2;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.Emitter;
import com.couchbase.lite.LiveQuery;
import com.couchbase.lite.Mapper;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.QueryRow;
import com.couchbase.lite.Reducer;
import com.couchbase.lite.View;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author julscol
 */
public class PrintQueue
{
    Database database = null;
    
    View printJobView = null;
    LiveQuery printJobQuery = null;
    
    View printerView = null;
    LiveQuery printerQuery = null;
    
    View printJobCountView = null;
    LiveQuery printJobCountQuery = null;
    
    ArrayList<Printer> printers = new ArrayList<Printer>();
    ArrayList<Document> printJobs = new ArrayList<Document>();
    
    public PrintQueue(Database database)
    {
        this.database = database;
        
        printerView = database.getView("print/printer");
        printerView.setMap(new Mapper() {
            @Override
            public void map(Map<String, Object> document, Emitter emitter)
            {
                Object doctype = document.get("doctype");
                Object isBluetooth = document.get("isBluetooth");
                Object isActive = document.get("isActive");
                Object isDeleted = document.get("isDeleted");
                Object printerOwner = document.get("printerOwner");
                
                if (doctype != null && "printer".equals((String)doctype) && isBluetooth != null && printerOwner == null)
                {
                    if ((isBluetooth instanceof Boolean && !((Boolean)isBluetooth)) || (isBluetooth instanceof Integer && ((int)isBluetooth) == 0))
                    {
                        if ((isActive instanceof Boolean && ((Boolean)isActive)) || (isActive instanceof Integer && ((int)isActive) != 0))
                        {
                            if ((isDeleted instanceof Boolean && !((Boolean)isDeleted)) || (isDeleted instanceof Integer && ((int)isDeleted) == 0))
                            {
                                emitter.emit(document.get("printerName"), document);
                            }
                        }
                    }
                }
            }
        }, "9");
        
        Query tempQuery = printerView.createQuery();
        
        try
        {
            printersChanged(tempQuery.run());
        }
        catch (CouchbaseLiteException ex)
        {
            Logger.getLogger(PrintQueue.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        printerQuery = tempQuery.toLiveQuery();
        printerQuery.addChangeListener(new LiveQuery.ChangeListener() {
            @Override
            public void changed(LiveQuery.ChangeEvent event) {
                if (event.getSource().equals(printerQuery)) {
                    printersChanged(event.getRows());
                }
            }
        });
        //printerQuery.start();
        
        
        printJobView = database.getView("print/print_job");
        printJobView.setMap(new Mapper() {
            @Override
            public void map(Map<String, Object> document, Emitter emitter)
            {
                Object doctype = document.get("doctype");
                Object printFrom = document.get("printFrom");
                if (doctype != null && "print_job".equals((String)doctype) && printFrom == null)
                {
                    Object printed = document.get("printed");
                    if (printed != null)
                    {
                        if (printed instanceof Boolean && !((boolean)printed))
                        {
                            emitter.emit(document.get("createdAt"), null);
                        }
                        else if (printed instanceof Integer && ((int)printed) == 0)
                        {
                            emitter.emit(document.get("createdAt"), null);
                        }
                    }
                }
            }
        }, "6");
        
        tempQuery = printJobView.createQuery();
        printJobQuery = tempQuery.toLiveQuery();
        printJobQuery.addChangeListener(new LiveQuery.ChangeListener() {
            @Override
            public void changed(LiveQuery.ChangeEvent event) {
                if (event.getSource().equals(printJobQuery)) {
                    printJobsChanged(event.getRows());
                }
            }
        });
        //printJobQuery.start();
        
        printJobCountView = database.getView("print/print_job_count");
        
        printJobCountView.setMapReduce(new Mapper() {

            @Override
            public void map(Map<String, Object> document, Emitter emitter)
            {
                Object doctype = document.get("doctype");
                Object printFrom = document.get("printFrom");
                if (doctype != null && "print_job".equals((String)doctype) && printFrom == null)
                {
                    Object printed = document.get("printed");
                    if (printed != null)
                    {
                        if (printed instanceof Boolean && !((boolean)printed))
                        {
                            emitter.emit(((String)document.get("printerName")) + "/" + ((String)document.get("ip")), null); // Ex. "Expo/192.168.0.1"
                        }
                        else if (printed instanceof Integer && ((int)printed) == 0)
                        {
                            emitter.emit(((String)document.get("printerName")) + "/" + ((String)document.get("ip")), null); // Ex. "Expo/192.168.0.1"
                        }
                    }
                }
            }
        }, new Reducer() {

            @Override
            public Object reduce(List<Object> list, List<Object> list1, boolean bln)
            {
                return list1.size();
            }
        }, "1");

    }
    
    public void printersChanged(QueryEnumerator qe)
    {
        
        
        // Put the query's results in an ArrayList
        ArrayList<Document> results = new ArrayList<>();
        for (QueryRow row: qe)
        {
            results.add(row.getDocument());
        }
        
        //LogHelper.dateLog("printers Changed... count: " + results.size());
        
        // Get the printerDocs of current printers
        ArrayList<Document> currentDocs = new ArrayList<>();
        for (Printer printer: printers)
        {
            currentDocs.add(printer.getPrinterDoc());
        }
        
        // Close and remove any printers that were not find in the latest query
        for (int x = 0; x < currentDocs.size(); x++)
        {
            if (!results.contains(currentDocs.get(x)))
            {
                currentDocs.remove(x);
                printers.remove(x);
                
                x--;
            }
        }
        
        // Add new printers that were not already in the list of current printers
        ArrayList<Printer> newPrinters = new ArrayList<>();
        for (Document doc: results)
        {
            if (!currentDocs.contains(doc))
            {
                Printer printer = makeNewPrinter(doc);

                if (printer != null)
                {
                    printers.add(printer);
                    newPrinters.add(printer);
                }
            }
        }
        
        // Update new printers with any printJobs that could be waiting on them
        for (Printer printer: newPrinters)
        {
            printer.updatePrintJobs(printJobs);
        }
    }
    
    private Printer makeNewPrinter(Document printerDoc)
    {
        Printer p = null;
        
        String model = (String)printerDoc.getProperty("printerModel");
        
        if (model.toLowerCase().contains("star"))
        {
            p = new PrinterStar(printerDoc, this);
        }
        else if (model.toLowerCase().contains("epson"))
        {
            p = new PrinterEpson(printerDoc, this);
        }
        else
        {
            LogHelper.dateLog("ERROR - Printer with unknown model found", LogHelper.ANSI_RED);
        }
        
        return p;
    }
    
    public void printJobsChanged(QueryEnumerator qe)
    {
        
        
        // Put the query's results in an ArrayList
        ArrayList<Document> results = new ArrayList<>();
        for (QueryRow row: qe)
        {
            results.add(row.getDocument());
        }
        
        //LogHelper.dateLog("printJobs Changed... count: " + results.size());
        
        // Remove printJobs that were not found in the latest query
        for (int x = 0; x < printJobs.size(); x++)
        {
            if (!results.contains(printJobs.get(x)))
            {
                printJobs.remove(x);
                x--;
            }
        }
        
        // Add new printJobs that were not already in the list of printJobs or list to be marked as printed
        for (Document doc : results)
        {
            if (!printJobs.contains(doc))
            {
                printJobs.add(doc);
            }
        }
        
        // Make sure that printers have the latest printJobs
        for (Printer printer: printers)
        {
            printer.updatePrintJobs(printJobs);
        }
        
    }
    
    public void updatePrinterStatus()
    {
        for (Printer printer: printers)
        {
            printer.updatePrinter();
            printer.updateStatus();
        }
        
        for (Printer printer: printers)
        {
            LogHelper.dateLog(printer.toString(), LogHelper.ANSI_GREEN);
        }
    }
    
    public void updatePrinters()
    {
        //LogHelper.dateLog("Querying for printers...");
        
       Query q = this.printerView.createQuery();
       
        try
        {
            QueryEnumerator qe = q.run();
            
            if (qe != null)
            {
                printersChanged(qe);
            }
        }
        catch (CouchbaseLiteException ex)
        {
            Logger.getLogger(PrintQueue.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updatePrintJobs()
    {
        //LogHelper.dateLog("Querying for print jobs...");
        
        Query blech = this.printJobCountView.createQuery();
        
        Query q = this.printJobView.createQuery();
        
        try
        {
            blech.run(); // Don't worry about results, just run it
            
            QueryEnumerator qe = q.run();
            
            if (qe != null)
            {
                printJobsChanged(qe);
            }
        }
        catch (CouchbaseLiteException ex)
        {
            Logger.getLogger(PrintQueue.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void savePrinterStatus()
    {
        ArrayList<Printer> tempPrinters = printers;
        for (Printer printer: tempPrinters)
        {
            
            Object statusObj = printer.getPrinterDoc().getProperty("printerStatus");
            
            if (statusObj != null)
            {
                String status = (String)statusObj;
                
                // if documen't status is not the current status, set it to current and save
                if (!status.equalsIgnoreCase(printer.status))
                {
                    Map<String, Object> props = new HashMap<String, Object>();
                    props.putAll(printer.getPrinterDoc().getProperties());

                    props.put("status", printer.status);

                    try
                    {
                        printer.getPrinterDoc().putProperties(props);
                    }
                    catch (CouchbaseLiteException ex)
                    {
                        Logger.getLogger(PrinterStar.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            
            
        }
    }
    
    public void printDocuments()
    {
        for (Printer printer: printers)
        {
            printer.print();
        }
    }
}
