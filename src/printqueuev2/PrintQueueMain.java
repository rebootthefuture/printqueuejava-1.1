/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package printqueuev2;

import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author julscol
 */
public class PrintQueueMain
{

    private static Timer timer = null;
    public static Boolean shouldExit = false;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        System.out.println("Args recieved: " + args.length);
        
        boolean shouldPrint = false;
        
        ArrayList<String> newArgs= new ArrayList();  
        for (int x = 0; x < args.length; x++)
        {
            newArgs.add(args[x]);
        }
        
        if (args.length > 0 && args[0].equalsIgnoreCase("true") || args[0].equalsIgnoreCase("false")) 
        {
            shouldPrint = Boolean.valueOf(args[0]);
            
            newArgs.remove(0);
        }
        
        int timeoutInterval = 10;
        
        if (newArgs.size() > 0)
        {
            try
            {
                timeoutInterval = Integer.valueOf(newArgs.get(0));
                
                newArgs.remove(0);
            }
            catch(NumberFormatException e) // It wasn't an integer, so ignore
            {
            }
        }
        
        if (timeoutInterval > 0)
        {
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                System.out.println("The system will reboot in 10 seconds! Finishing print jobs...");
                PrintQueueMain.shouldExit = true;
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                    System.out.println("Rebooting...");
                    System.exit(0);
                    }
                }, 10*1000);
                }
            }, timeoutInterval*60*1000);
            LogHelper.dateLog("System scheduled to reboot in " + timeoutInterval + " minute(s)", LogHelper.ANSI_PURPLE);
        }
        
        ServerHandler sh = ServerHandler.server(newArgs.toArray(args));
        
        if (shouldPrint)
        {
            LogHelper.dateLog("Printing Enabled!!", LogHelper.ANSI_RED);
            
            PrintQueue pq = new PrintQueue(sh.database);

            Date lastStatusUpdate = new Date();

            pq.updatePrinterStatus();

            Date waitDate = new Date();
            Date newNow = new Date();
            while (newNow.getTime() - waitDate.getTime() < 1000)
            {
                newNow = new Date();
            }

            while (true)
            {
                pq.updatePrinters();
                
                pq.updatePrintJobs();
                
                Date now = new Date();

                if (now.getTime() - lastStatusUpdate.getTime() > 10000)
                {
                    LogHelper.dateLog("********    Checking printer Status    ********");
                    pq.updatePrinterStatus();
                    lastStatusUpdate = new Date();

                    // Wait a second after checking the status to allow for ports to be closed
                    waitDate = new Date();
                    newNow = new Date();
                    while (newNow.getTime() - waitDate.getTime() < 1000)
                    {
                        newNow = new Date();
                    }
                }

                pq.printDocuments();

                pq.savePrinterStatus();
            }
        }
        else
        {
            LogHelper.dateLog("Printing Disabled!!", LogHelper.ANSI_RED);
        }
        
    }
    
}
