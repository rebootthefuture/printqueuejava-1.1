/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package printqueuev2;

import com.couchbase.lite.Attachment;
import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Document;
import com.couchbase.lite.Revision;
import com.couchbase.lite.UnsavedRevision;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTableDataCell;
import java.io.*;
import java.net.*;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;

/**
 *
 * @author julscol
 */
public class PrinterEpson extends Printer
{
    private String link = "";
    
    public PrinterEpson(Document printerDoc, PrintQueue printQueue)
    {
        super(printerDoc, printQueue);
        
        link = "http://epson:epson@" + IPAddress + "/istatus.htm";
    }
    
    @Override
    public void updateStatus()
    {
        String newStatus = "Unreachable";
        
        try
        {
            newStatus = queryStatus();
        }
        catch (IOException ex)
        {
            String something = "";
        }
        
        updateStatus(newStatus);
    }
    
    private String queryStatus() throws IOException
    {
        String result = "Unreachable";
        
        WebClient webClient = new WebClient(BrowserVersion.CHROME);
        webClient.getOptions().setUseInsecureSSL(true);
        webClient.getOptions().setTimeout(2000);
        HtmlPage page = webClient.getPage(link);

        List<?> cells = page.getByXPath("//td[@bgcolor='#009900']");
        List<?> cells2 = page.getByXPath("//td[@bgcolor='#ffff00']");
        List<?> cells3 = page.getByXPath("//td[@id='status-error']");
        List<?> cells4 = page.getByXPath("//td[@id='status-normal']");

        HtmlTableDataCell cell = null;

        if (cells.size() > 0)
        {
            cell = (HtmlTableDataCell) cells.get(0);
        }
        else if (cells2.size() > 0)
        {
            cell = (HtmlTableDataCell) cells2.get(0);
        }
        else if (cells3.size() > 0)
        {
            cell = (HtmlTableDataCell) cells3.get(0);
        }
        else if (cells4.size() > 0)
        {
            cell = (HtmlTableDataCell) cells4.get(0);
        }

        if (cell != null)
        {
            result = cell.getTextContent();
        }

        webClient.close();
        
        return result;
    }
    
    @Override
    protected void printDocument(Document printJob)
    {
        if (!printing)
        {
            printing = true;
            
            boolean printJobCompleted = false;

            LogHelper.dateLog("Printing " + printJob.getId() + " to " + IPAddress + " - " +  printerName + "(" + printerModel + ")...");
            
            if (status != null)
            {
                // If status is 'Online'
                if ("Online".equalsIgnoreCase(status))
                {
                    Object printString = printJob.getProperty("printString");
                        
                    // Try to use a printString if available
                    if (printString != null && !"".equals((String)printString))
                    {
                        LogHelper.dateLog("Using printString!!!", LogHelper.ANSI_WHITE);
                        
                        try 
                        {
                            //byte[] command = ((String)printString).getBytes(Charset.forName("UTF-8"));
                            byte[] command = new sun.misc.BASE64Decoder().decodeBuffer((String)printString);
                            
                            int count = 0;
                            for (byte b : command)
                            {
                                if (b == 0x07)
                                {
                                    command[count] = 0x00;
                                }
                                
                                count++;
                            }

                            // Initialize data transfer objects
                            ByteArrayOutputStream writer = new ByteArrayOutputStream();
                            Socket socket = new Socket(IPAddress, 9100, true);
                            DataInputStream socketInput = null;
                            socketInput = new DataInputStream(socket.getInputStream());

                            // Wite data to printer
                            writer.write(command);
                            writer.writeTo(socket.getOutputStream());

                            // Completed without exception, mark printed
                            printJobCompleted = true;
                            printJobs.remove(printJob);
                        }
                        catch (IOException ex)
                        {
                            LogHelper.dateLog("Socket Error - " + ex.getMessage(), LogHelper.ANSI_RED);
                            updateStatus("Unreachable");
                        }
                        
                    }
                    else
                    {
                        Revision rev = printJob.getCurrentRevision();
                        Attachment att = rev.getAttachment(((String)printJob.getProperty("attachmentName")));

                        // Check that the attachment is valid
                        if (att != null)
                        {
                            try 
                            {
                                // Prep attachment for transfer
                                InputStream is = att.getContent();
                                byte[] command = IOUtils.toByteArray(is);

                                // Initialize data transfer objects
                                ByteArrayOutputStream writer = new ByteArrayOutputStream();
                                Socket socket = new Socket(IPAddress, 9100, true);
                                DataInputStream socketInput = null;
                                socketInput = new DataInputStream(socket.getInputStream());

                                // Wite data to printer
                                writer.write(command);
                                writer.writeTo(socket.getOutputStream());

                                // Completed without exception, mark printed
                                printJobCompleted = true;
                                printJobs.remove(printJob);
                            }  
                            catch (CouchbaseLiteException ex) 
                            {
                                LogHelper.dateLog("Error - " + ex.getMessage(), LogHelper.ANSI_RED);
                            }
                            catch (IOException ex)
                            {
                                LogHelper.dateLog("Socket Error - " + ex.getMessage(), LogHelper.ANSI_RED);
                                updateStatus("Unreachable");
                            }
                        }
                        else
                        {
                            LogHelper.dateLog("Error Printing - The printJob's attachment is not ready", LogHelper.ANSI_RED);
                        }
                    }
                    
                    
                }
                else
                {
                    updateStatus(status);
                    LogHelper.dateLog("Error Printing - " + status, LogHelper.ANSI_RED);
                }
            }
            else
            {
                updateStatus("Unreachable");
                LogHelper.dateLog("Error Printing - " + printerName + " is unreachable", LogHelper.ANSI_RED);
            }
            
            // If print was successful, save changes
            if (printJobCompleted)
            {
                Map<String, Object> props = new HashMap<String, Object>();
                props.putAll(printJob.getProperties());

                props.put("printed", true);
                
                
                try
                {
                    String timerStr = "";
                    TimeZone tz = TimeZone.getTimeZone("UTC");
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
                    df.setTimeZone(tz);
                    Date now = new Date();
                    Date then = new DateTime((String)props.get("createdAt")).toDate();
                    
                    String nowAsISO = df.format(now);
                    
                    Double timeDiff = (now.getTime() - then.getTime()) / 1000.0;
                    
                    timerStr += "created: " + (String)props.get("createdAt") + " printed: " + nowAsISO + " " + LogHelper.ANSI_RED + timeDiff + " seconds" + LogHelper.ANSI_RESET;
                    
                    props.put("printedAt", nowAsISO);
                    
                    UnsavedRevision newRev = printJob.getCurrentRevision().createRevision();
                    
                    newRev.removeAttachment((String)printJob.getProperty("attachmentName"));
                    newRev.setProperties(props);
                    newRev.save();
                    
                    LogHelper.dateLog(printJob.getId() + " marked as printed... " + timerStr);
                }
                catch (CouchbaseLiteException ex)
                {
                    Logger.getLogger(PrinterStar.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            // If print failed, move job to the end of list
            if (!printJobCompleted && printJobs.contains(printJob))
            {
                printJobs.remove(printJob);
                printJobs.add(printJob);
            }
            
            printing = false;
        }
    }
    
}
