/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package printqueuev2;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.View;
import com.couchbase.lite.Database;
import com.couchbase.lite.Database.ChangeListener;
import com.couchbase.lite.Document;
import com.couchbase.lite.DocumentChange;
import com.couchbase.lite.JavaContext;
import com.couchbase.lite.Manager;
import com.couchbase.lite.SavedRevision;
import com.couchbase.lite.TransactionalTask;
import com.couchbase.lite.UnsavedRevision;
import com.couchbase.lite.javascript.JavaScriptViewCompiler;
import com.couchbase.lite.listener.Credentials;
import com.couchbase.lite.listener.LiteListener;
import com.couchbase.lite.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author julscol
 */
public class ServerHandler
{
    private static ServerHandler server = null;
    
    Manager manager = null;
    Database database = null;
    
    CBServerConnection localConnection = null;
    CBServerConnection cloudConnection = null;
    
    LiteListener listener = null;
    
    private ServerHandler(String[] args)
    {
        if (args.length < 1)
        {
            System.out.println("You must include the local server IP Address");
            System.exit(1);
        }
        else
        {
            String localUrlStr = args[0];
            
            if(args.length > 1 && args[1] != null)
            {
                if ("cloud".equals(args[1].toLowerCase()))
                {
                    if (args.length > 2)
                    {
                        setData(localUrlStr, true, args[2]);
                    }
                    else
                    {
                        System.out.println("To use 'cloud', you must include the cloud server IP Address");
                        System.exit(1);
                    }
                }
                else
                {
                    System.out.println("Unexpected command '" + args[1] + "'");
                    System.exit(1);
                }
            }
            else
            {
                setData(localUrlStr, false, "");
            }
        }
    }
    
    static public ServerHandler server(String[] args)
    {
        Boolean shouldMakeNew = (server == null);
        
        if(shouldMakeNew)
        {
            server = new ServerHandler(args);
        }
          
        return server;
    }
    
    private ServerHandler(String localUrlStr, Boolean shouldReplicateCloud, String cloudUrlStr)
    {
        setData(localUrlStr, shouldReplicateCloud, cloudUrlStr);
    }
    
    private void setData(String localUrlStr, Boolean shouldReplicateCloud, String cloudUrlStr)
    {
        try 
        {
            Manager.enableLogging(Log.TAG_SYNC, Log.ASSERT);
            Manager.enableLogging(Log.TAG_ROUTER, Log.ASSERT);
            Manager.enableLogging(Log.TAG_CHANGE_TRACKER, Log.ERROR);
            Manager.enableLogging(Log.TAG_REMOTE_REQUEST, Log.ASSERT);
            Manager.enableLogging(Log.TAG_BATCHER, Log.ERROR);
            
            View.setCompiler(new JavaScriptViewCompiler());
            
            //CBLView.setCompiler(new CBLJavaScriptViewCompiler());
            
            String[] splits = localUrlStr.split("/");
            String dbName = splits[splits.length - 1];
            
            if (dbName.contains("bucket"))
            {
                dbName = dbName + "_" + getUsername(localUrlStr) + "_" + getPassword(localUrlStr);
            }
            
            dbName = dbName.toLowerCase();
            
            //View.setCompiler(new JavaScriptViewCompiler());
            manager = new Manager(new JavaContext("data"), Manager.DEFAULT_OPTIONS);
            database = manager.getDatabase(dbName);
            
//            database.addChangeListener(new ChangeListener() {
//
//                @Override
//                public void changed(Database.ChangeEvent ce)
//                {
//                    for (DocumentChange docChange : ce.getChanges())
//                    {
//                        Document doc = database.getDocument(docChange.getDocumentId());
//                        
//                        if (doc != null)
//                        {
//                            try
//                            {
//                                List<SavedRevision> conflicts = doc.getConflictingRevisions();
//                                
//                                if (conflicts.size() > 1)
//                                {
//                                    LogHelper.dateLog("Document " + doc.getId() + " has conflicts... Resolving...", LogHelper.ANSI_YELLOW);
//                                    
//                                    database.runInTransaction(new TransactionalTask() {
//
//                                        @Override
//                                        public boolean run()
//                                        {
//                                            try
//                                            {
//                                                Map<String, Object> newProps = getLatestProps(conflicts);
//                                                
//                                                SavedRevision current = doc.getCurrentRevision();
//                                                
//                                                for (SavedRevision rev : conflicts)
//                                                {
//                                                    UnsavedRevision newRev = rev.createRevision();
//                                                    if (rev.getId().equals(current.getId()))
//                                                    {
//                                                        newRev.setProperties(newProps);
//                                                    }
//                                                    else
//                                                    {
//                                                        newRev.setIsDeletion(true);
//                                                    }
//                                                    
//                                                    newRev.save(true);
//                                                }
//                                            }
//                                            catch (CouchbaseLiteException ex)
//                                            {
//                                                return false;
//                                            }
//                                            
//                                            return true;
//                                        }
//                                        private Map<String, Object> getLatestProps(List<SavedRevision> conflicts)
//                                        {
//                                            Map<String, Object> latest = null;
//                                            ArrayList<SavedRevision> cons = new ArrayList();
//                                            
//                                            // Get all non-deleted revisions
//                                            for (SavedRevision rev : conflicts)
//                                            {
//                                                if (!rev.getProperties().containsKey("_deleted"))
//                                                {
//                                                    cons.add(rev);
//                                                }
//                                            }
//                                            
//                                            // There is more than 1 non-deleted revision, narrow it down
//                                            if (cons.size() > 1)
//                                            {
//                                                // Remove all revs without basic properties
//                                                for (int x = 0; x < cons.size(); x++)
//                                                {
//                                                    // If it doesn't contain 'doctype' of 'isDeleted', it is a bad revsion
//                                                    if (!cons.get(x).getProperties().containsKey("doctype") || !cons.get(x).getProperties().containsKey("isDeleted"))
//                                                    {
//                                                        cons.remove(x);
//                                                        x--;
//                                                    }
//                                                }
//                                                
//                                                // There are still more than 1 available revisions, filter based on 'updatedAt'
//                                                if (cons.size() > 1)
//                                                {
//                                                    for (SavedRevision rev : cons)
//                                                    {
//                                                        if (!latest.containsKey("updatedAt")) // does not have updatedAt
//                                                        {
//                                                            if (rev.getProperties().containsKey("updatedAt")) // if rev has it, assign it
//                                                            {
//                                                                latest = rev.getProperties();
//                                                            }
//                                                        }
//                                                        else if (rev.getProperties().containsKey("updatedAt")) // both have updatedAt
//                                                        {
//                                                            if ( ((String)(rev.getProperties().get("updatedAt"))).compareTo((String)(latest.get("updatedAt"))) > 0) // rev has a newer updatedAt
//                                                            {
//                                                                latest = rev.getProperties(); // sat to rev's props
//                                                            }
//                                                        }
//                                                    }
//                                                }
//                                                // There is now only 1 valid revision, pick it
//                                                else if (cons.size() == 1)
//                                                {
//                                                    latest = cons.get(0).getProperties();
//                                                }
//                                                // There are no valid revisions left, pick the default
//                                                else
//                                                {
//                                                    latest = conflicts.get(0).getProperties();
//                                                }
//                                            }
//                                            // There is 1 non-deleted revision, pick it
//                                            else if (cons.size() == 1)
//                                            {
//                                                latest = cons.get(0).getProperties();
//                                            }
//                                            // There are no non-deleted revisions, pick the default
//                                            else
//                                            {
//                                                latest = conflicts.get(0).getProperties();
//                                            }
//                                            
//                                            
//                                            return latest;
//                                        }
//                                    });
//                                }
//                            }
//                            catch (CouchbaseLiteException ex)
//                            {
//                            }
//                        }
//                    }
//                }
//            });
            
            database.open();
            
            listener = new LiteListener(manager, 10000, new Credentials("asd", "123"));
            listener.start();
            
            localConnection = new CBServerConnection(database, localUrlStr, true, true);
            
            if (shouldReplicateCloud)
            {
                cloudConnection = new CBServerConnection(database, cloudUrlStr, true, true);
            }      
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(ServerHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private String getUsername(String urlStr)
    {
        String temp = urlStr;
        int index = -1;
        
        index = temp.indexOf("@");
        
        temp = temp.substring(0, index);
        temp = temp.replace("http://", "");
        
        index = temp.indexOf(":");
        
        temp = temp.substring(0, index);
        
        return temp;
    }
    
    private String getPassword(String urlStr)
    {
        String temp = urlStr;
        int index = -1;
        
        index = temp.indexOf("@");
        
        temp = temp.substring(0, index);
        temp = temp.replace("http://", "");
        
        index = temp.indexOf(":");
        
        temp = temp.substring(index+1);
        
        return temp;
    }
    
}
