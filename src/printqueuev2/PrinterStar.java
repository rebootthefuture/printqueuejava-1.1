/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package printqueuev2;

import com.couchbase.lite.Attachment;
import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Document;
import com.couchbase.lite.Revision;
import com.couchbase.lite.UnsavedRevision;
import com.starmicronics.stario.StarIOPort;
import com.starmicronics.stario.StarIOPortException;
import com.starmicronics.stario.StarPrinterStatus;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;

/**
 *
 * @author julscol
 */
public class PrinterStar extends Printer
{

    public PrinterStar(Document printerDoc, PrintQueue printQueue)
    {
        super(printerDoc, printQueue);
    }
    
    @Override
    public void updateStatus()
    {
        StarIOPort starPort = null;
        // Open a connection to the printer
        
        String localIP = IPAddress;
        
//        localIP = "TCP:192.168.1.99";
//        
//        if (printerModel.toLowerCase().contains("sp700"))
//        {
//            localIP = "TCP:192.168.22.107";
//        }
        
        try
        {
            starPort = StarIOPort.getPort(localIP, "", 2000);
        }
        catch (StarIOPortException ex)
        {
            LogHelper.dateLog("Could not open port at " + localIP + " - " + ex.getMessage(), LogHelper.ANSI_RED);
        }

        // If the connection was good
        if (starPort != null)
        {
            StarPrinterStatus status = null;

            // Query the status of the printer
            try
            {
                status = starPort.retreiveStatus();
            }
            catch (StarIOPortException ex)
            {
                Logger.getLogger(PrintQueue.class.getName()).log(Level.SEVERE, null, ex);
            }

            // If the status is valid
            if (status != null)
            {
                boolean canContinue = true;

                if (status.coverOpen)
                {
                    canContinue = false;
                    updateStatus("Cover Open");
                }
                if (status.cutterError)
                {
                    canContinue = false;
                    updateStatus("Cutter Error");
                }
                if (status.overTemp)
                {
                    canContinue = false;
                    updateStatus("Overheating");
                }
                if (status.receiptPaperEmpty)
                {
                   canContinue = false; 
                   updateStatus("Paper Empty");
                }
                if (status.receiveBufferOverflow)
                {
                    canContinue = false;
                    updateStatus("Buffer Overflow");
                }
                if (status.unrecoverableError)
                {
                   canContinue = false; 
                   updateStatus("Unrecoverable Error");
                }

                // If no issues were found
                if (canContinue)
                {
                    updateStatus("Online");
                }
            }
            else
            {
                updateStatus("Unreachable");
            }
            
            try 
            {
                StarIOPort.releasePort(starPort);
            } 
            catch (StarIOPortException ex) 
            {
                LogHelper.dateLog("Error - " + ex.getMessage(), LogHelper.ANSI_RED);
            }
        }
        else
        {
            updateStatus("Unreachable");
        }
    }
    
    @Override
    protected void printDocument(Document printJob)
    {
        if (!printing)
        {
            printing = true;
        
            boolean printJobCompleted = false;

            StarIOPort starPort = null;
            
            String localIP = IPAddress;

            LogHelper.dateLog("Printing " + printJob.getId() + " to " + localIP + " - " + printerName + "(" + printerModel + ")...");

            // Open a connection to the printer
            try
            {
                starPort = StarIOPort.getPort(localIP, "", 2000);
            }
            catch (StarIOPortException ex)
            {
                LogHelper.dateLog("Could not open port at " + localIP + " - " + ex.getMessage(), LogHelper.ANSI_RED);
            }

            // If the connection was good
            if (starPort != null)
            {
                StarPrinterStatus status = null;

                // Query the status of the printer
                try
                {
                    status = starPort.retreiveStatus();
                }
                catch (StarIOPortException ex)
                {
                    Logger.getLogger(PrintQueue.class.getName()).log(Level.SEVERE, null, ex);
                }

                // If the status is valid
                if (status != null)
                {
                    boolean canContinue = true;

                    if (status.coverOpen)
                    {
                        canContinue = false;
                        LogHelper.dateLog("Error Printing - Printer cover open", LogHelper.ANSI_RED);
                        updateStatus("Cover Open");
                    }
                    if (status.cutterError)
                    {
                        canContinue = false;
                        LogHelper.dateLog("Error Printing - Cutter error", LogHelper.ANSI_RED);
                        updateStatus("Cutter Error");
                    }
                    if (status.overTemp)
                    {
                        canContinue = false;
                        LogHelper.dateLog("Error Printing - Printer is too hot", LogHelper.ANSI_RED);
                        updateStatus("Overheating");
                    }
                    if (status.receiptPaperEmpty)
                    {
                       canContinue = false; 
                       LogHelper.dateLog("Error Printing - Paper is empty", LogHelper.ANSI_RED);
                       updateStatus("Paper Empty");
                    }
                    if (status.receiveBufferOverflow)
                    {
                        canContinue = false;
                        LogHelper.dateLog("Error Printing - Printer buffer overflow", LogHelper.ANSI_RED);
                        updateStatus("Buffer Overflow");
                    }
                    if (status.unrecoverableError)
                    {
                       canContinue = false; 
                       LogHelper.dateLog("Error Printing - Printer error is unrecoverable", LogHelper.ANSI_RED);
                       updateStatus("Unrecoverable Error");
                    }

                    // If no issues were found
                    if (canContinue)
                    {
                        updateStatus("Online");
                        
                        Object printString = printJob.getProperty("printString");
                        
                        // Try to use a printString if available
                        if (printString != null && !"".equals((String)printString))
                        {
                            LogHelper.dateLog("Using printString!!!", LogHelper.ANSI_WHITE);
                            // Try to transmit printJob
                            try 
                            {
                                //byte[] command = ((String)printString).getBytes(Charset.forName("UTF-8"));
                                byte[] command = new sun.misc.BASE64Decoder().decodeBuffer((String)printString);
                                
                                int count = 0;
                                for (byte b : command)
                                {
                                    if (b == 0x07)
                                    {
                                        command[count] = 0x00;
                                    }

                                    count++;
                                }

                                //StarPrinterStatus status2 = starPort.beginCheckedBlock();

                                starPort.writePort(command, 0, command.length);

                                starPort.setEndCheckedBlockTimeoutMillis(60000);

                                //status2 = starPort.endCheckedBlock();

                                printJobCompleted = true;
                                printJobs.remove(printJob);
                            }
                            catch (StarIOPortException ex) 
                            {
                                LogHelper.dateLog("Could not write to port - " + ex.getMessage(), LogHelper.ANSI_RED);
                            }
                            catch (IOException ex)
                            {
                                LogHelper.dateLog("Decoding Error - " + ex.getMessage(), LogHelper.ANSI_RED);
                            }
                        }
                        else
                        {
                            Revision rev = printJob.getCurrentRevision();
                            Attachment att = rev.getAttachment(((String)printJob.getProperty("attachmentName")));

                            // If attachment is available
                            if (att != null)
                            {
                                // Try to transmit printJob
                                try 
                                {
                                    InputStream is = att.getContent();
                                    byte[] command = IOUtils.toByteArray(is);

                                    //StarPrinterStatus status2 = starPort.beginCheckedBlock();

                                    starPort.writePort(command, 0, command.length);

                                    starPort.setEndCheckedBlockTimeoutMillis(60000);

                                    //status2 = starPort.endCheckedBlock();

                                    printJobCompleted = true;
                                    printJobs.remove(printJob);
                                }  
                                catch (CouchbaseLiteException | IOException ex) 
                                {
                                    LogHelper.dateLog("Error - " + ex.getMessage(), LogHelper.ANSI_RED);
                                } 
                                catch (StarIOPortException ex) 
                                {
                                    LogHelper.dateLog("Could not write to port - " + ex.getMessage(), LogHelper.ANSI_RED);
                                }
                            }

                            try 
                            {
                                StarIOPort.releasePort(starPort);
                            } 
                            catch (StarIOPortException ex) 
                            {
                                LogHelper.dateLog("Error - " + ex.getMessage(), LogHelper.ANSI_RED);
                            }
                        }
                    }
                }
                else
                {
                    updateStatus("Unreachable");
                    LogHelper.dateLog("Error Printing - " + printerName + " is unreachable", LogHelper.ANSI_RED);
                }
            }
            else
            {
                updateStatus("Unreachable");
                LogHelper.dateLog("Error Printing - " + printerName + " is unreachable", LogHelper.ANSI_RED);
            }

            // If print was successful, save changes
            if (printJobCompleted)
            {
                Map<String, Object> props = new HashMap<String, Object>();
                props.putAll(printJob.getProperties());

                props.put("printed", true);
                
                try
                {
                    String timerStr = "";
                    TimeZone tz = TimeZone.getTimeZone("UTC");
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                    df.setTimeZone(tz);
                    Date now = new Date();
                    Date then = new DateTime((String)props.get("createdAt")).toDate();

                    String nowAsISO = df.format(now);
                    
                    Double timeDiff = (now.getTime() - then.getTime()) / 1000.0;
                    
                    timerStr += "created: " + (String)props.get("createdAt") + " printed: " + nowAsISO + " " + LogHelper.ANSI_RED + timeDiff + " seconds" + LogHelper.ANSI_RESET;
                    
                    props.put("printedAt", nowAsISO);
                    
                    UnsavedRevision newRev = printJob.getCurrentRevision().createRevision();
                    
                    newRev.removeAttachment((String)printJob.getProperty("attachmentName"));
                    newRev.setProperties(props);
                    newRev.save();
                    
                    LogHelper.dateLog(printJob.getId() + " marked as printed... " + timerStr);
                }
                catch (CouchbaseLiteException ex)
                {
                    Logger.getLogger(PrinterStar.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            // If print failed, move job to the end of list
            if (!printJobCompleted && printJobs.contains(printJob))
            {
                printJobs.remove(printJob);
                printJobs.add(printJob);
            }
            
            printing = false;
        }
    }
}
