/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package printqueuev2;

import com.couchbase.lite.Document;
import java.util.ArrayList;

/**
 *
 * @author julscol
 */
public class Printer
{
    
    protected String IPAddress = "0.0.0.0";
    protected Boolean isActive = false;
    
    protected String printerModel = "";
    protected String printerName = "";
    
    protected Document printerDoc = null;

    protected String status = "Unreachable";
    
    protected final ArrayList<Document> printJobs = new ArrayList<Document>();
    
    protected Boolean printing = false;
    
    protected PrintQueue printQueue = null;
    
    public Printer(Document printerDoc, PrintQueue printQueue)
    {
        this.printerDoc = printerDoc;
        this.printQueue = printQueue;
        
        IPAddress = (String)printerDoc.getProperty("IPAddress");
        isActive = (Boolean)printerDoc.getProperty("isActive");
        
        printerModel = (String)printerDoc.getProperty("printerModel");
        printerName = (String)printerDoc.getProperty("printerName");
    }
    
    public void updatePrinter()
    {
        if (printerDoc != null)
        {
            IPAddress = (String)printerDoc.getProperty("IPAddress");
            isActive = (Boolean)printerDoc.getProperty("isActive");
            
            printerModel = (String)printerDoc.getProperty("printerModel");
            printerName = (String)printerDoc.getProperty("printerName");
        }
    }
    
    public Document getPrinterDoc()
    {
        return printerDoc;
    }
    
    public void updatePrintJobs(ArrayList<Document> printJobs)
    {
        // Check to see if any printJobs have been removed
        for(int x = 0; x < this.printJobs.size(); x++)
        {
            // If print jobs do not exist in new set, remove them from queue
            if(!printJobs.contains(this.printJobs.get(x)))
            {
                this.printJobs.remove(x);
                x--;
            }
        }
        
        // Add print jobs to queue if they do not already exist
        for (Document printJob : printJobs)
        {
            // If print jobs do not exist in current set and they belong to the current printer, add them to queue
            if (this.printerName.equalsIgnoreCase((String)printJob.getProperty("printerName")) && 
                    this.IPAddress.equalsIgnoreCase((String)printJob.getProperty("ip")) && !this.printJobs.contains(printJob))
            {
                this.printJobs.add(printJob);
            }
        }
        
        //LogHelper.dateLog(toString(), LogHelper.ANSI_PURPLE);
    }
    
    protected void updateStatus(String status)
    {
        this.status = status; 
    }
    
    public void updateStatus()
    {
        
    }
    
    @Override
    public String toString()
    {
        String str = "";
        
        str += printerName + " - " + printerModel;
        str += " - (" + IPAddress + ") *** ";
        
        if (status.equalsIgnoreCase("online"))
        {
            str += "Status: " + status + "; Jobs: " + printJobs.size();
        }
        else
        {
            str += LogHelper.ANSI_RED + "Status: " + status + "; Jobs: " + printJobs.size();
        }
        
        
        return str;
    }
    
    public void print()
    {
        Document printJob = getNextPrintJob();
        
        if (printJob != null)
        {
            printDocument(printJob);
        }
        
    }
    
    protected Document getNextPrintJob()
    {
        if (PrintQueueMain.shouldExit)
        {
            return null;
        }
        
        // has print jobs and is online
        if (printJobs.size() > 0 && "Online".equalsIgnoreCase(status))
        {
            return printJobs.get(0);
        }
        
        // has print jobs, but cant print
        if (printJobs.size() > 0 && !"Online".equalsIgnoreCase(status))
        {
            // print failure message here
            //LogHelper.dateLog(printerName + "(" + printerModel + ") failed to print... Print Jobs: " + printJobs.size());
        }
        
        // if there are no jobs or the printer is not available, return null
        return null;
    }
    
    protected void printDocument(Document printJob)
    {
        if (!printing)
        {
            printing = true;
            
            LogHelper.dateLog("Printing doc " + printJob.getId() + "...");
            
            printing = false;
        }
    }
}
