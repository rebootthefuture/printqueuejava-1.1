/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package printqueuev2;

import com.couchbase.lite.Database;
import com.couchbase.lite.replicator.Replication;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;


/**
 *
 * @author julscol
 */
public class CBServerConnection
{
    private Replication push;
    private Replication pull;
    
    public CBServerConnection(Database database, String urlStr, Boolean shouldPush, Boolean shouldPull)
    {
        try
        {
            URL url = new URL(urlStr);
            
            push = database.createPushReplication(url);
            pull = database.createPullReplication(url);
            
            push.setContinuous(true);
            pull.setContinuous(true);
            
            push.addChangeListener(new Replication.ChangeListener() 
            {
                @Override
                public void changed(Replication.ChangeEvent event) 
                {
                    Throwable err = push.getLastError();
                    
                    LogHelper.dateLog("Push for " + push.getRemoteUrl() + ": " + push.getStatus() + ", " + push.getCompletedChangesCount() + "/" + push.getChangesCount());

                    if (err != null)
                    {
                        LogHelper.dateLog(push.getRemoteUrl() + " Push error: " + err.getMessage(), LogHelper.ANSI_RED);
                        LogHelper.dateLog("Restarting push replication", LogHelper.ANSI_RED);
                        push.restart();
                    }
                }
            });
            pull.addChangeListener(new Replication.ChangeListener() 
            {
                @Override
                public void changed(Replication.ChangeEvent event) 
                {
                    Throwable err = pull.getLastError();

                    LogHelper.dateLog("Pull for " + pull.getRemoteUrl() + ": " + pull.getStatus() + ", " + pull.getCompletedChangesCount() + "/" + pull.getChangesCount());
                    
                    if (err != null && false)
                    {
                        LogHelper.dateLog(pull.getRemoteUrl() + " Pull error: " + err.getMessage(), LogHelper.ANSI_RED);
                        LogHelper.dateLog("Restarting pull replication", LogHelper.ANSI_RED);
                        pull.restart();
                    }
                }
            });
            
            List<String> channels = new ArrayList<String>();

            DateFormat df = new SimpleDateFormat("yyMMdd");
            Calendar startCal = Calendar.getInstance();
            startCal.add(Calendar.DAY_OF_YEAR, -1);
            
            channels.add(getUsername(urlStr) + "_" + getPassword(urlStr) + "_vital");
            for (int x = 0; x < 3; x++)
            {
                Date date = startCal.getTime();
                channels.add(getUsername(urlStr) + "_" + getPassword(urlStr) + "_" + df.format(date));
                startCal.add(Calendar.DAY_OF_YEAR, 1);
            }
            
            channels = new ArrayList<String>();
            channels.add(getUsername(urlStr) + "_" + getPassword(urlStr) + "_print");
            
            pull.setChannels(channels);
            
            if(shouldPush)
            {
                push.start();
            }
            if(shouldPull)
            {
                pull.start();
            }
            
            LogHelper.dateLog("Connection created for " + urlStr + ". push: " + shouldPush + " pull: " + shouldPull);
        }
        catch (MalformedURLException ex)
        {
            Logger.getLogger(CBServerConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void startAllReplication()
    {
        startPushReplication();
        startPullReplication();
    }
    public void stopAllReplication()
    {
        stopPushReplication();
        stopPullReplication();
    }
    
    public void startPushReplication()
    {
        push.start();
    }
    public void stopPushReplication()
    {
        push.stop();
    }
    
    public void startPullReplication()
    {
        pull.start();
    }
    public void stopPullReplication()
    {
        pull.stop();
    }
    
    private String getUsername(String urlStr)
    {
        String temp = urlStr;
        int index = -1;
        
        index = temp.indexOf("@");
        
        temp = temp.substring(0, index);
        temp = temp.replace("http://", "");
        
        index = temp.indexOf(":");
        
        temp = temp.substring(0, index);
        
        return temp;
    }
    
    private String getPassword(String urlStr)
    {
        String temp = urlStr;
        int index = -1;
        
        index = temp.indexOf("@");
        
        temp = temp.substring(0, index);
        temp = temp.replace("http://", "");
        
        index = temp.indexOf(":");
        
        temp = temp.substring(index+1);
        
        return temp;
    }
}
